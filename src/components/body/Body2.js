import React from "react";

const Body2 = () => (
  <section id="presentation">
    <div className="container">
      <div className="partieIllus">
        <img
          className="image portrait"
          src="/assets/images/portrait9.png"
          alt="illustration me representant"
        ></img>
        <h2 className="nom title title2">Marie-Noëlle</h2>
      </div>
      <div className="partieTexte">
        <h1 className="nom title title2">Je vous parle de moi</h1>
        <h2 className="citation">
          "Pendant des années, j'ai attendu que ma vie change. Mais maintenant
          je sais que c'était elle qui attendait que moi je change."{" "}
          <span>Fabio Volo</span>
        </h2>
        <div className="boxes">
          <p className="box p1">
            J'ai obtenue le titre de Développeuse web en 2021 et de Conceptrice
            Développeuse d'Application en décembre 2023, je suis aujourd'hui à
            la recherche d'une entreprise capable de me faire évoluer rapidement
            tout en profitant de mes multiples talents. Multipotentielle
            passionnée, je suis capable d'apprendre rapidement et de faire
            preuve de créativité pour résoudre tout ce qui me sera présenté.
            <br></br>
          </p>
          <p className="box p2">
            Pendant les 15 mois de mon contrat de professionnalisation, j’ai eu
            la chance de travailler sur différents projets. J'ai pu crée une
            application de A à Z en Symphony 6 avec PHP 8 et la livrer à
            l'entreprise. Elle est aujourd'hui en production et fais la joie de
            ses utilisateurs. J'ai aussi travaillé sur un formulaire en React.
            Le back était sur Symfony 4.4 et PHP 7.4 J'ai bien sur utilisé
            abondamment gitlab et grandement gagné en compétence sur l'anglais
            technique. Un très bon niveau d'anglais me permet de pouvoir
            utiliser la documentation technique en anglais sans problème.
          </p>
          <p className="box p3">
            Aujourd'hui, je n'ai qu'une hâte : trouver une entreprise qui me
            permette de mettre en pratique ce fascinant métier !
          </p>
        </div>
      </div>
    </div>
  </section>
);

export default Body2;
