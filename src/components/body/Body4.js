import React from "react";

const Body4 = () => (
  <section id="search">
    <div className="pageUnique">
      <img
        className="image illusDip"
        src="/assets/images/cherche.png"
        alt="illustration de moi montrant les diplomes"
      ></img>
      <div className="boxes">
        <div className="title title2">
          Un poste de conceptrice d'application web et web mobile<br></br>
        </div>
        <p>
          Cette reconversion entamé un peu au hasard m'a mené sur la voie du
          Développement Web. L'expérience au sein de la société Fareva m'a fait
          comprendre que j'ai autant besoin de créativité que de défi. Un emploi
          me permettant d'allier les deux serait l'idéal. Idéalement, je
          voudrais travailler dans une entreprise qui produit des jeux, que ce
          soit des serious game ou bien des jeux mobile, ou même soyons fou, des
          jeux vidéos. J'ai toujours été passionnée par la création de jeu, et
          je serai ravie de pouvoir participer à leurs création.
          <br></br>
          Même si je n'ai pas suivi le cursus idéal pour travailler dans ce
          domaine, je me forme en autodidacte depuis longtemps sur un grand
          nombre de sujet qui me permettrait d'être rapidement efficace sur ce
          type d'emploi. Soyez audacieux, tentez l'aventure !
        </p>
      </div>
      {/* <a className="btn btn-white" href="#contact">Contact</a> */}
    </div>
  </section>
);

export default Body4;
