import React from "react";
import AppRouter from "./router";
import Style from "./scss/App.scss";

class App extends React.Component {
  render() {
    return (
      <>
        <AppRouter />
      </>
    );
  }
}

export default App;
